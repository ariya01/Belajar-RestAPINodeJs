var express = require('express');
var router = express.Router();
const data = require('../model/getDrama');

router.get('/:page', function(req, res, next) {
    var page = req.params.page;
    data.getDrama(function(request,result){
        const pageCount = Math.ceil(result.length / 10);
        res.json({
            pageTotal : pageCount,
            page : parseInt(page),
            data : result.slice(page * 10 - 10, page * 10),
            message: "success",
            code: 200
          });
    });
});

router.get('/aktor/:id', function(req, res, next) {
    var id = req.params.id;
    data.getActor(id,function(request,result){
        res.json({
            data : result,
            message: "success",
            code: 200
          });
    });
});

router.get('/cast/:id', function(req, res, next) {
    var id = req.params.id;
    data.getCast(id,function(request,result){
        res.json({
            data : result,
            message: "success",
            code: 200
          });
    });
});

router.get('/drama/:id', function(req, res, next) {
    var id = req.params.id;
    var detail;
    var cast=[];
    var episode;
    data.getDetailDrama(id,function(request1,result1){
        detail = result1;
        data.getEpisode(id,function(request3,result3){
            episode = result3;
            data.getDetailCast(id,function(request2,result2)
            {
                cast= result2;
                res.json({
                    data : {
                        Drama : detail,
                        Episodenya : episode,
                        Cast : cast
                    },
                    message: "success",
                    code: 200
                  });
            });    
            
        });
      });
});

router.post('/cari/',function(req,res,next)
{
    var query = req.body.query;
    data.getSearch(query,function(request,result){
        console.log(result);
    });
});
module.exports = router;
