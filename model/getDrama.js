var db = require("../config/mysql");

const model ={
	getDrama:function (callback) {
		return db.query('select * from Daftar',callback);
	},
	getActor:function(id,callback){
		return db.query('select * from Aktor WHERE id_aktor = ?',[id],callback);
	},
	getCast:function(id,callback){
		return db.query('select * from Cast WHERE id_drakor = ?',parseInt([id]),callback);
	},
	getDetailDrama:function (id,callback) {
		return db.query('select * from Daftar Where id_drakor = ?',[id],callback);
	},
	getEpisode:function (id,callback) {
		return db.query('select * from Episode Where id_drakor = ?',[id],callback);
	},
	getDetailCast:function(id,callback){
		return db.query('select * from Cast Left join Aktor on Cast.id_aktor = Aktor.id_aktor where id_drakor = ?',parseInt([id]),callback);
	},
	getSearch:function(string,callback)
	{
		data = '%' + string + '%';
		return db.query('select * from Daftar WHERE nama_drakor LIKE ?',[data],callback);
	}
}

module.exports = model;
