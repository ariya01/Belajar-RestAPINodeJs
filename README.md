# Belajar-RestAPINodeJs

Menggunakan Framework Express Js

Pada Project ini saya menggunakan Model dan Route untuk pembuatan REST API

Port yang digunakan adalah 8000
### RestAPI yang bisa digunakan

1. URL = `BaseURL`+/v1/
    
    Method = `GET` 
    
    Keterangan = Digunakan untuk mengecek API

2. URL = `BaseURL` +/v1/+ data/:page

    Method = `GET`

    Keterangan = Digunakan untuk mengambil Semua Drama

3. URL = `BaseURL` +/v1/+ data/aktor/:id

    Method = `GET`

    Keterangan = Digunakan untuk melihat Detail Aktor

4. URL = `BaseURL` +/v1/+ data/cast/:id

    Method = `GET`

    Keterangan = Digunakan untuk melihat pemain yang bermain di Drama tersebut

5. URL = `BaseURL` +/v1/+ data/drama/:id

    Method = `GET`

    Keterangan = Digunakan untuk melihat Detail Drama

6. URL = `BaseURL` +/v1/+data/cari

    Method = `POST`

    Body = query

    Keterangan = Digunakan untuk mencari Drama

### Note
Jangan lupa untuk bikin .env dengan isi sseperti ini dan sesuaikan dengan SQL masing-masing

* DB_HOST = localhost
* DB_USER = "root"
* DB_PASS = ""
* DB_NAME = "text"